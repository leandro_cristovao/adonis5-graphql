//https://docs.adonisjs.com/cookbooks/testing-adonisjs-apps#testing-http-calls
// import User from 'App/Models/User'
import test from 'japa'
// import { login } from './utils'
const { login, send, createUser } = require('./utils.ts')

test.group('Users', () => {
  test('Listar', async (assert) => {
    await createUser()
    const token = await login()
    const query = `{
    User(searchParam:{
      filters:[
        {
          field:"id"
          operator:">"
          value:"1"
        }
      ]
      orders:[
        {
          field:"id"
          order: DESC
        }
      ]
      pagination:{
        page:1 
        perPage: 20
      }
    }){
      pages{
        total 
        perPage 
        page 
        lastPage
      }
      rows{
        id 
        email
      }
    }
  }`

    const response = await send(query, token.token)
    assert.equal(response.statusCode, 200)

    const body = response.body
    assert.exists(body.data.User)
    assert.notProperty(body, 'errors')
  })

  test('Criar', async (assert) => {
    const token = await login()
    const query = `mutation {
        upsertUser(input: {
          name: "Meu usuário"
          email: "teste@terra.com.br", 
          password: "123456"}) {
          id
          email
        }
      }
      `

    const response = await send(query, token.token)
    assert.equal(response.statusCode, 200)

    const body = response.body
    assert.exists(body.data.upsertUser)
    assert.notProperty(body, 'errors')
  })

  test('Editar', async (assert) => {
    const user = await createUser()
    const token = await login()
    const query = `mutation {
        upsertUser(input: {
          id: ${user.id} 
          name: "${user.name}" 
          email: "${user.email}"}) {
          id
          email
        }
      }
      `

    const response = await send(query, token.token)
    assert.equal(response.statusCode, 200)

    const body = response.body
    assert.exists(body.data.upsertUser)
    assert.notProperty(body, 'errors')
  })

  test('Criar - Campo Inválido', async (assert) => {
    const token = await login()
    const query = `mutation {
        upsertUser(input: {
          nao_existe: "teste@terra.com.br", 
          password: "123456"}) {
          id
          email
        }
      }
      `

    const response = await send(query, token.token)
    assert.equal(response.statusCode, 400)

    const body = response.body
    assert.exists(body.errors)
  })
})
