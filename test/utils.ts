import Answer from 'App/Models/Answer'
import Question from 'App/Models/Question'
import User from 'App/Models/User'
import supertest from 'supertest'

const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}`
const ADMIN_EMAIL = process.env.ADMIN_EMAIL
const ADMIN_PWD = process.env.ADMIN_PWD

async function login() {
  const response = await supertest(BASE_URL)
    .post('/login')
    .set('Accept', 'application/json')
    .expect('Content-Type', /json/)
    .send({
      email: ADMIN_EMAIL,
      password: ADMIN_PWD,
    })
    .expect(200)

  return response.body
}

async function send(query, token) {
  const response = await supertest(BASE_URL)
    .post('/graphql')
    .set('Authorization', `Bearer ${token}`)
    .set('Accept', 'application/json')
    .type('form')
    .send({ query })

  return response
}

async function createUser() {
  const entity = new User()
  entity.name = 'name'
  entity.email = 'email@terra.com.br'
  entity.password = '123456'
  await entity.save()

  return entity
}

async function createQuestion() {
  const entity = new Question()
  entity.name = 'Minha Question'
  await entity.save()
  await createAnswer(entity.id)

  return entity
}

async function createAnswer(question_id) {
  const entity = new Answer()
  entity.name = 'Minha Question'
  entity.question = question_id
  await entity.save()

  return entity
}

export = {
  createUser,
  createAnswer,
  createQuestion,
  login,
  send,
}
