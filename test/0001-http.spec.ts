//https://docs.adonisjs.com/cookbooks/testing-adonisjs-apps#testing-http-calls
import Question from 'App/Models/Question'
import test from 'japa'
import supertest from 'supertest'
const { login, send } = require('./utils.ts')
const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}`
const ADMIN_EMAIL = process.env.ADMIN_EMAIL
const ADMIN_PWD = process.env.ADMIN_PWD

test.group('HTTP Methods', () => {
  test('Login - Válido', async (assert) => {
    const response = await supertest(BASE_URL)
      .post('/login')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .send({
        email: ADMIN_EMAIL,
        password: ADMIN_PWD,
      })
      .expect(200)

    const body = response.body
    assert.exists(body.token)
    assert.equal(body.type, 'bearer')
  })

  test('Login - Inválido', async () => {
    try {
      const qqqqq = await Question.query().preload('skills').first()
      console.log(qqqqq?.toJSON())
    } catch (ex) {
      console.log(ex)
    }
    await supertest(BASE_URL)
      .post('/login')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .send({
        email: 'email_invalido@terra.com',
        password: '123456',
      })
      .expect(401)
  })

  test('Graphql - Com sessão', async (assert) => {
    const token = await login()

    const query = `{
      __type(name: "User") {
        name
      },
    }`
    const response = await send(query, token.token)
    assert.equal(response.statusCode, 200)
  })

  test('Graphql - Sem sessão', async (assert) => {
    const query = `{
      __type(name: "User") {
        name
      },
    }`
    const response = await send(query, '')
    assert.equal(response.statusCode, 401)
  })
})
