//https://docs.adonisjs.com/cookbooks/testing-adonisjs-apps#testing-http-calls
import test from 'japa'
// import { login } from './utils'
const { login, send, createQuestion } = require('./utils.ts')

test.group('Questions', () => {
  test('Listar', async (assert) => {
    await createQuestion()
    const token = await login()
    const query = `{
    Question(searchParam:{
      filters:[
        {
          field:"id"
          operator:"="
          value:"1"
        }
      ]
      orders:[]
      pagination:{
        page:1 
        perPage: 20
      }
    }){
      pages{
        total 
        perPage 
        page 
        lastPage
      }
      rows{
        id 
        name
      }
    }
  }`

    const response = await send(query, token.token)
    assert.equal(response.statusCode, 200)

    const body = response.body
    assert.exists(body.data.Question)
    assert.notProperty(body, 'errors')
  })

  test('Criar', async (assert) => {
    const token = await login()
    const query = `mutation {
        upsertQuestion(input: {
          name: "Minha Question"}) {
          id
          name
        }
      }
      `

    const response = await send(query, token.token)
    assert.equal(response.statusCode, 200)

    const body = response.body
    assert.exists(body.data.upsertQuestion)
    assert.notProperty(body, 'errors')
  })

  test('Editar', async (assert) => {
    const entity = await createQuestion()
    const token = await login()
    const query = `mutation {
        upsertQuestion(input: {
          id: ${entity.id} 
          name: "${entity.name}"}) {
          id
          name
        }
      }
      `

    const response = await send(query, token.token)
    assert.equal(response.statusCode, 200)

    const body = response.body
    assert.exists(body.data.upsertQuestion)
    assert.notProperty(body, 'errors')
  })
})
