//https://docs.adonisjs.com/cookbooks/testing-adonisjs-apps#testing-http-calls
import User from 'App/Models/User'
import BaseRepository from 'App/Repositories/BaseRepository'
import test from 'japa'

const repository = new BaseRepository(User)
const auth = {
  user: {
    id: 1,
    tenant_id: 1,
  },
}

test.group('Search Params', () => {
  test('@NULL', async (assert) => {
    const searchParam = {
      filters: [
        {
          field: 'email',
          operator: '=',
          value: '@null',
        },
      ],
      pagination: {
        page: 1,
        perPage: 20,
      },
    }

    const data = await repository.paginate(searchParam, auth)
    assert.exists(data.rows)
  })

  test('@FALSE', async (assert) => {
    const searchParam = {
      filters: [
        {
          field: 'email',
          operator: '=',
          value: '@false',
        },
      ],
      pagination: {
        page: 1,
        perPage: 20,
      },
    }

    const data = await repository.paginate(searchParam, auth)
    assert.exists(data.rows)
  })

  test('@TRUE', async (assert) => {
    const searchParam = {
      filters: [
        {
          field: 'email',
          operator: '=',
          value: '@true',
        },
      ],
      pagination: {
        page: 1,
        perPage: 20,
      },
    }

    const data = await repository.paginate(searchParam, auth)
    assert.exists(data.rows)
  })

  test('Operador IN', async (assert) => {
    const searchParam = {
      filters: [
        {
          field: 'id',
          operator: 'IN',
          value: [3],
        },
      ],
      pagination: {
        page: 1,
        perPage: 20,
      },
    }

    const data = await repository.paginate(searchParam, auth)
    assert.exists(data.rows)
  })

  test('Operador IN - Aninhado', async (assert) => {
    const params = {
      searchParam: {
        filters: [
          {
            field: 'cboPrerequisiteAges.id',
            operator: 'IN',
            value: [3],
          },
        ],
        pagination: {
          page: 1,
          perPage: 20,
        },
      },
    }

    const data = await repository.paginate(params, auth)
    assert.nestedProperty(data, 'rows')
    assert.notProperty(data, 'errors')
  })

  test('Operador NOT_IN - Array', async (assert) => {
    const params = {
      searchParam: {
        filters: [
          {
            field: 'id',
            operator: 'NOT_IN',
            value: [1],
          },
        ],
        orders: [
          {
            field: 'id',
            order: 'ASC',
          },
        ],
        pagination: {
          page: 1,
          perPage: 1,
        },
      },
    }

    const data = await repository.paginate(params, auth)
    assert.exists(data.rows)
  })

  test('Operador NOT_IN - String', async (assert) => {
    const params = {
      searchParam: {
        filters: [
          {
            field: 'id',
            operator: 'NOT_IN',
            value: '1,2',
          },
        ],
        orders: [
          {
            field: 'id',
            order: 'ASC',
          },
        ],
        pagination: {
          page: 1,
          perPage: 1,
        },
      },
    }

    const data = await repository.paginate(params, auth)
    assert.exists(data.rows)
  })

  // test('Operador NOT_IN - Aninhado', async (assert) => {
  //   const params = {
  //     searchParam: {
  //       filters: [
  //         {
  //           field: 'cboPrerequisiteAges.id',
  //           operator: 'NOT_IN',
  //           value: [1],
  //         },
  //       ],
  //       orders: [
  //         {
  //           field: 'id',
  //           order: 'ASC',
  //         },
  //       ],
  //       pagination: {
  //         page: 1,
  //         perPage: 1,
  //       },
  //     },
  //   }

  //   const data = await repository.paginate(params, auth)
  //   assert.nestedProperty(data, 'rows')
  //   assert.notProperty(data, 'errors')
  // })

  // test('Operador IS_NULL', async (assert) => {
  //   const params = {
  //     searchParam: {
  //       showDeleted: true,
  //       filters: [
  //         {
  //           field: "print_logo_id",
  //           operator: "in",
  //           value: "36728",
  //         },
  //         // {
  //         //   field: "printLogo.id",
  //         //   operator: "!=",
  //         //   value: "36725"
  //         // },
  //         {
  //           field: 'chairman_administrative_council_signature_id',
  //           operator: 'IS_NULL',
  //           value: '',
  //         },
  //         {
  //           field: 'id',
  //           operator: 'IS_NULL',
  //           value: '',
  //         },
  //         {
  //           field: "chairmanAdministrativeCouncilSignature.tenant_id",
  //           operator: "=",
  //           value: "1"
  //         },
  //       ],
  //       orders: [
  //         {
  //           field: 'id',
  //           order: 'ASC',
  //         },
  //       ],
  //       pagination: {
  //         page: 1,
  //         perPage: 1,
  //       },
  //     },
  //   }

  //   const data = await repository.paginate(params, auth)
  //   assert.nestedProperty(data, 'rows')
  //   assert.notProperty(data, 'errors')
  // })

  // test('Operador IS_NULL - Aninhado', async (assert) => {
  //   const params = {
  //     searchParam: {
  //       filters: [
  //         {
  //           field: 'cboPrerequisiteAges.id',
  //           operator: 'IS_NULL',
  //           value: '',
  //         },
  //       ],
  //       orders: [
  //         {
  //           field: 'id',
  //           order: 'ASC',
  //         },
  //       ],
  //       pagination: {
  //         page: 1,
  //         perPage: 1,
  //       },
  //     },
  //   }

  //   const data = await repository.paginate(params, auth)
  //   assert.nestedProperty(data, 'rows')
  //   assert.notProperty(data, 'errors')
  // })

  // test('Operador IS_NOT_NULL', async (assert) => {
  //   const params = {
  //     searchParam: {
  //       filters: [
  //         {
  //           field: 'id',
  //           operator: 'IS_NOT_NULL',
  //           value: '',
  //         },
  //         {
  //           field: 'city_id',
  //           operator: 'IS_NOT_NULL',
  //           value: ''
  //         }
  //       ],
  //       orders: [
  //         {
  //           field: 'id',
  //           order: 'ASC',
  //         },
  //         {
  //           field: 'city.name',
  //           order: 'DESC'
  //         }
  //       ],
  //     },
  //   }

  //   const data = await repository.paginate(params, auth)
  //   assert.nestedProperty(data, 'rows')
  //   assert.notProperty(data, 'errors')
  // })

  // test('Operador IS_NOT_NULL - Aninhado', async (assert) => {
  //   const params = {
  //     searchParam: {
  //       filters: [
  //         {
  //           field: 'cboPrerequisiteAges.id',
  //           operator: 'IS_NOT_NULL',
  //           value: '',
  //         },
  //       ],
  //       orders: [
  //         {
  //           field: 'id',
  //           order: 'ASC',
  //         },
  //       ],
  //       pagination: {
  //         page: 1,
  //         perPage: 1,
  //       },
  //     },
  //   }

  //   const data = await repository.paginate(params, auth)
  //   assert.nestedProperty(data, 'rows')
  //   assert.notProperty(data, 'errors')
  // })

  // test('Operador HAS', async (assert) => {
  //   const params = {
  //     searchParam: {
  //       filters: [
  //         {
  //           field: 'cboPrerequisiteAges',
  //           operator: 'HAS',
  //           value: '',
  //         },
  //         {
  //           field: 'chairmanAdministrativeCouncilSignature.tenant',
  //           operator: 'HAS',
  //           value: '',
  //         },
  //       ],
  //       orders: [
  //         {
  //           field: 'id',
  //           order: 'ASC',
  //         },
  //       ],
  //       pagination: {
  //         page: 1,
  //         perPage: 1,
  //       },
  //     },
  //   }

  //   const data = await repository.paginate(params, auth)
  //   assert.nestedProperty(data, 'rows')
  //   assert.notProperty(data, 'errors')
  // })

  // test('Operador DOESNT_HAVE', async (assert) => {
  //   const params = {
  //     searchParam: {
  //       filters: [
  //         {
  //           field: 'cboPrerequisiteAges',
  //           operator: 'DOESNT_HAVE',
  //           value: '',
  //         },
  //       ],
  //       orders: [
  //         {
  //           field: 'id',
  //           order: 'ASC',
  //         },
  //       ],
  //       pagination: {
  //         page: 1,
  //         perPage: 1,
  //       },
  //     },
  //   }

  //   const data = await repository.paginate(params, auth)
  //   assert.nestedProperty(data, 'rows')
  //   assert.notProperty(data, 'errors')
  // })

  // // test('Operador DOESNT_HAVE with value', async (assert) => {
  // //   const params = {
  // //     searchParam: {
  // //       filters: [
  // //         {
  // //           field: 'nationalExecutiveSecretarySENARSignature.id',
  // //           operator: 'DOESNT_HAVE',
  // //           value: '30077',
  // //         },
  // //       ],
  // //       orders: [
  // //         {
  // //           field: 'id',
  // //           order: 'ASC',
  // //         },
  // //       ],
  // //       pagination: {
  // //         page: 1,
  // //         perPage: 1,
  // //       },
  // //     },
  // //   }

  // //   const data = await repository.paginate(params, auth)
  // //   assert.nestedProperty(data, 'rows')
  // //   assert.notProperty(data, 'errors')
  // // })

  // test('Operador HAS_MORE', async (assert) => {
  //   const params = {
  //     searchParam: {
  //       filters: [
  //         {
  //           field: 'cboPrerequisiteAges',
  //           operator: 'HAS_MORE',
  //           value: '1',
  //         },
  //       ],
  //       orders: [
  //         {
  //           field: 'id',
  //           order: 'ASC',
  //         },
  //       ],
  //       pagination: {
  //         page: 1,
  //         perPage: 1,
  //       },
  //     },
  //   }

  //   const data = await repository.paginate(params, auth)
  //   assert.nestedProperty(data, 'rows')
  //   assert.notProperty(data, 'errors')
  // })

  // test('Operador HAS_MORE_EQUAL', async (assert) => {
  //   const params = {
  //     searchParam: {
  //       filters: [
  //         {
  //           field: 'cboPrerequisiteAges',
  //           operator: 'HAS_MORE_EQUAL',
  //           value: '1',
  //         },
  //       ],
  //       orders: [
  //         {
  //           field: 'id',
  //           order: 'ASC',
  //         },
  //       ],
  //       pagination: {
  //         page: 1,
  //         perPage: 1,
  //       },
  //     },
  //   }

  //   const data = await repository.paginate(params, auth)
  //   assert.nestedProperty(data, 'rows')
  //   assert.notProperty(data, 'errors')
  // })

  // test('Operador filter by Field (field >= @field)', async (assert) => {
  //   const params = {
  //     searchParam: {
  //       filters: [
  //         {
  //           field: 'id',
  //           operator: '>=',
  //           value: '@id',
  //         },
  //       ],
  //       orders: [
  //         {
  //           field: 'id',
  //           order: 'ASC',
  //         },
  //       ],
  //       pagination: {
  //         page: 1,
  //         perPage: 1,
  //       },
  //     },
  //   }

  //   const data = await repository.paginate(params, auth)
  //   assert.nestedProperty(data, 'rows')
  //   assert.notProperty(data, 'errors')
  // })
})
