import BaseSchema from '@ioc:Adonis/Lucid/Schema'
import utils from '../utils'

export default class Roles extends BaseSchema {
  protected tableName = 'roles'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      utils.baseTable(table)
      table.string('slug').notNullable()

      table.unique(['slug', 'tenant_id'])
    })

    this.schema.alterTable('users', (table) => {
      table.integer('role_id').unsigned().references('id').inTable('roles')
    })

    this.defer(async (db) => {
      await db.table(this.tableName).insert([
        { id: 1, created_user_id: 1, name: 'Administrador', slug: 'ADMIN' },
        { id: 2, created_user_id: 1, name: 'Monitor', slug: 'MONITOR' },
        { id: 3, created_user_id: 1, name: 'Tutor Interno', slug: 'INTERNAL_TUTOR' },
        { id: 4, created_user_id: 1, name: 'Tutor Externo', slug: 'EXTERNAL_TUTOR' },
        { id: 5, created_user_id: 1, name: 'Revisor', slug: 'REVISOR' },
        {
          id: 6,
          created_user_id: 1,
          name: 'Tutor Avaliador Interno',
          slug: 'INTERNAL_EVALUATOR_TUTOR',
        },
        {
          id: 7,
          created_user_id: 1,
          name: 'Tutor Avaliador Externo',
          slug: 'EXTERNAL_EVALUATOR_TUTOR',
        },
      ])
    })
  }

  public async down() {
    this.schema.alterTable('users', (table) => {
      table.dropForeign('role_id')
      table.dropColumn('role_id')
    })
    this.schema.dropTable(this.tableName)
  }
}
