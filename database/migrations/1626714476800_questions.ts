import BaseSchema from '@ioc:Adonis/Lucid/Schema'
import utils from '../utils'

export default class Questions extends BaseSchema {
  protected tableName = 'questions'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      utils.baseTable(table)
      table.string('contentType', 15).notNullable().defaultTo('TEXT')
      table.string('level', 15).notNullable().defaultTo('BASIC')
      table.string('reference', 45).comment('Código usado no EXCEL')
      table.string('status', 15).notNullable().defaultTo('NEW')
      table.string('type', 15).notNullable().defaultTo('OBJECTIVE')
      table.text('content', 'longtext')

      table.boolean('isNew').notNullable().defaultTo(0)
      table.integer('lastSemester')

      table.integer('course_id').unsigned().references('courses.id')
      table.integer('course_unit_id').unsigned().references('course_units.id')
      table.integer('evaluation_type_id').unsigned().references('evaluation_types.id')
    })

    this.defer(async (db) => {
      await db.table(this.tableName).insert({
        id: 1,
        name: 'name',
        content: 'content@dotgroup.com.br',
        reference: 'excel',
      })
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
