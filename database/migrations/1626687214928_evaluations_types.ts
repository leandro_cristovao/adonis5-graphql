import BaseSchema from '@ioc:Adonis/Lucid/Schema'
import utils from '../utils'
export default class EvaluationTypes extends BaseSchema {
  protected tableName = 'evaluation_types'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      utils.baseTable(table)
      table.integer('totalQuestions')
      table.integer('grade')
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
