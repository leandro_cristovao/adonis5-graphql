import BaseSchema from '@ioc:Adonis/Lucid/Schema'
import utils from '../utils'
export default class Skills extends BaseSchema {
  protected tableName = 'skills'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      utils.baseTable(table)
    })

    this.defer(async (db) => {
      await db.table(this.tableName).insert({
        id: 1,
        name: 'Skill - 01',
      })
      await db.table(this.tableName).insert({
        id: 2,
        name: 'Skill - 02',
      })
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
