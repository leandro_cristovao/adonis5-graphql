import BaseSchema from '@ioc:Adonis/Lucid/Schema'
import utils from '../utils'

export default class Courses extends BaseSchema {
  protected tableName = 'courses'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      utils.baseTable(table)
    })

    this.defer(async (db) => {
      await db.table(this.tableName).insert([
        {
          id: 1,
          name: 'Curso - 01',
        },
        {
          id: 2,
          name: 'Curso - 02',
        },
      ])
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
