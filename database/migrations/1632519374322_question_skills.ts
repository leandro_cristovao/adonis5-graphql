import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class QuestionSkills extends BaseSchema {
  protected tableName = 'question_skills'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')

      table.integer('question_id').unsigned().references('id').inTable('questions')
      table.integer('skill_id').unsigned().references('id').inTable('skills')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })

    this.defer(async (db) => {
      await db.table(this.tableName).insert({
        question_id: 1,
        skill_id: 1,
      })
      await db.table(this.tableName).insert({
        question_id: 1,
        skill_id: 2,
      })
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
