import BaseSchema from '@ioc:Adonis/Lucid/Schema'
import utils from '../utils'
export default class CourseSubjects extends BaseSchema {
  protected tableName = 'course_units'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      utils.baseTable(table)
      table.string('slug').notNullable()
    })

    this.defer(async (db) => {
      await db.table(this.tableName).insert([
        {
          id: 1,
          name: 'Course Unit - 01',
          slug: 'CU1',
        },
      ])
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
