import BaseSchema from '@ioc:Adonis/Lucid/Schema'
import utils from '../utils'
export default class Topics extends BaseSchema {
  protected tableName = 'topics'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      utils.baseTable(table)

      table.integer('level').defaultTo(1).notNullable()
      table.integer('course_unit_id').unsigned().references('id').inTable('course_units')
      table
        .integer('parent_id')
        .unsigned()
        .references('id')
        .inTable('topics')
        .defaultTo(1)
        .notNullable()
    })

    this.defer(async (db) => {
      await db.table(this.tableName).insert({
        id: 1,
        name: 'Topic - 01',
      })
      await db.table(this.tableName).insert({
        id: 2,
        name: 'Topic - 02',
      })
      await db.table(this.tableName).insert({
        id: 3,
        name: 'Topic - 03',
      })
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
