import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class CourseUnits extends BaseSchema {
  protected tableName = 'course_unit_topics'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')

      table.integer('course_unit_id').unsigned().references('id').inTable('course_units')
      table.integer('topic_id').unsigned().references('id').inTable('topics')
      table.unique(['course_unit_id', 'topic_id'])

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
