import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Tenants extends BaseSchema {
  protected tableName = 'tenants'

  public async up() {
    const exits = await this.schema.hasTable(this.tableName)
    if (!exits) {
      this.schema.createTable(this.tableName, (table) => {
        table.increments('id')
        table.string('name', 255).notNullable()

        /**
         * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
         */
        table.timestamp('created_at', { useTz: true })
        table.timestamp('updated_at', { useTz: true })
      })

      this.defer(async (db) => {
        await db.table(this.tableName).insert({ id: 1, name: 'DOT Digital Group' })
      })
    }
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
