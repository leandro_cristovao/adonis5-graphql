import BaseSchema from '@ioc:Adonis/Lucid/Schema'
import utils from '../utils'

export default class Answers extends BaseSchema {
  protected tableName = 'answers'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      utils.baseTable(table)

      table.integer('question_id').unsigned().references('questions.id')
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
