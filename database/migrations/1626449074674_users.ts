import BaseSchema from '@ioc:Adonis/Lucid/Schema'
import Hash from '@ioc:Adonis/Core/Hash'
import utils from '../utils'

export default class UsersSchema extends BaseSchema {
  1
  protected tableName = 'users'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      utils.baseTable(table)
      table.string('email', 255).notNullable()
      table.string('password', 180).nullable()
      table.string('remember_me_token').nullable()
    })

    this.defer(async (db) => {
      const password = await Hash.make('123456')
      await db.table(this.tableName).insert({
        id: 1,
        name: 'DOT Digital Group',
        email: 'admin@dotgroup.com.br',
        password,
      })
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
