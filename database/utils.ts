export = {
  baseTable(table: any) {
    table.increments('id')
    table.string('name', 255).notNullable()

    table
      .integer('created_user_id')
      .unsigned()
      .references('id')
      .inTable('users')
      .defaultTo(1)
      .notNullable()
    table.integer('updated_user_id').unsigned().references('id').inTable('users')
    table.integer('deleted_user_id').unsigned().references('id').inTable('users')
    table.boolean('deleted').defaultTo(false).notNullable()
    table
      .integer('tenant_id')
      .unsigned()
      .references('id')
      .inTable('tenants')
      .defaultTo(1)
      .notNullable()

    /**
     * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
     */
    table.timestamp('createdAt', { useTz: true })
    table.timestamp('updatedAt', { useTz: true })
    table.timestamp('deletedAt', { useTz: true })
  },
}
