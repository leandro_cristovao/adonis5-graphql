import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Skill from 'App/Models/Skill'

export default class SkillSeeder extends BaseSeeder {
  public async run() {
    const uniqueKey = 'id'

    await Skill.updateOrCreateMany(uniqueKey, [
      {
        id: 1,
        name: 'Skill 01',
        tenant_id: 1,
        created_user_id: 1,
      },
      {
        id: 2,
        name: 'Skill 02',
        tenant_id: 1,
        created_user_id: 1,
      },
    ])
  }
}
