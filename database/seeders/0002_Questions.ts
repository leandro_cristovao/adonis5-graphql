import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Question from 'App/Models/Question'

export default class QuestionSeeder extends BaseSeeder {
  public async run() {
    const uniqueKey = 'id'

    await Question.updateOrCreateMany(uniqueKey, [
      {
        id: 1,
        name: 'name',
        content: 'content@dotgroup.com.br',
        reference: 'excel',
      },
    ])
  }
}
