FROM node:14-alpine AS build
RUN mkdir -p /home/node/app/node_modules
WORKDIR /home/node/app
COPY package.json ./
RUN apk add --no-cache git
COPY . /home/node/app/
RUN rm /home/node/app/package-lock.json
RUN chown -R node:node /home/node
RUN npm install && npm run build
USER node
EXPOSE 3333
ENTRYPOINT ["node","ace","serve","--watch"]