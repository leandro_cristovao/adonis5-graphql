Core AdonisJS 5 + GraphQL + MySQL + Unit Tests + Docker + Repository Pattern

# Instalação

## Requisitos para instalação

    git
    node 14
    npm 6
    mysql

    docker
    docker-compose

## Configuração

Criar dois bancos de dados

    create database questions_database CHARACTER SET utf8 COLLATE utf8_general_ci;
    create database questions_database_test CHARACTER SET utf8 COLLATE utf8_general_ci;

Renomeie o arquivo `.env.example` para `.env`, ajuste os as configurações de acesso ao banco de dados.

Rodar o comando

    npm install
    node ace migration:run

Parar rodar a aplicação, executar `node ace serve`

## Executando no Docker

Rodar o comando

    docker-compose up

# Testes Unitários

Para rodar os testes

    npm test

Para rodar um único arquivo de teste, executar

    npm test 0001-http.spec.ts
