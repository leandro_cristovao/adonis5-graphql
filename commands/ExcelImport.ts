import { args, BaseCommand } from '@adonisjs/core/build/standalone'

// class ExcelRow {
//   reference: string;
//   tema: string;
//   questionType: string;
//   level: string;

//   constructor(row: string[]) {
//     this.reference = row[0];
//     this.tema = row[1];
//     this.questionType = row[2]
//     this.level = row[3];
//   }

//   greet() {
//     return "Hello, " + this.greeting;
//   }
// }

export default class ExcelImport extends BaseCommand {
  /**
   * Command name is used to run the command
   */
  public static commandName = 'excel:import'

  @args.string({ description: 'Caminho para o arquivo Excel' })
  public path: string

  /**
   * Command description is displayed in the "help" output
   */
  public static description =
    'Importador de madeira para tentar fazer alguma espécie de mágica para importar arquivos Excel/Word'

  public static settings = {
    /**
     * Set the following value to true, if you want to load the application
     * before running the command
     */
    loadApp: false,

    /**
     * Set the following value to true, if you want this command to keep running until
     * you manually decide to exit the process
     */
    stayAlive: false,
  }

  public async run() {
    const readXlsxFile = require('read-excel-file/node')

    // // const { default: User } = await import('App/Models/User')
    // // await User.create()

    this.logger.info(`Lendo arquivo ${this.path}`)

    const reader = await readXlsxFile(this.path)
    for (let row of reader) {
      console.log(row)
    }
    // console.log(reader)
    // readXlsxFile(this.path, { schema }).then(({ rows, errors }) => {
    //   // `errors` list items have shape: `{ row, column, error, value }`.

    //   console.log(errors.length)
    //   console.log(rows)
    // })
  }
}
