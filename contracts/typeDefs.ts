import User from 'App/Models/User'

declare module '@ioc:Adonis/Addons/Auth' {
  interface AuthManagerContract {
    user: User
  }
}

declare module '@ioc:Adonis/Lucid/Orm' {
  interface ModelPaginatorContract<Result extends LucidRow> {
    rows: []
  }
}
