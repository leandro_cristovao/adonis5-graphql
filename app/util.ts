import Database from '@ioc:Adonis/Lucid/Database'
async function upsertWithTransaction(repository, input, auth) {
  const trx = await Database.transaction()
  try {
    let entity = {}
    if (!input.id) {
      entity = await repository.create(input, auth, trx)
    } else {
      entity = await repository.merge(input, auth, trx)
    }
    // @ts-ignore
    await entity.save(trx)
    await trx.commit()
    return entity
  } catch (ex) {
    await trx.rollback()
    throw ex
  }
}

function prepareQuery(query, searchParam) {
  if (!searchParam.pagination) {
    searchParam.pagination = {
      page: 1,
      perPage: 50,
    }
  }

  if (searchParam.orders) {
    let relation = [...searchParam.orders].map((f) => f.field)
    relation = [...new Set(relation)]
    relation.map((f) => {
      if (isSubfield(f)) {
        let relation = formatSubfield(f, query.model)
        query.modify((queryBuilder) => {
          queryBuilder.leftJoin(
            relation.tableName,
            `${query.model.table}.${relation.relation}_id`,
            `${relation.tableName}.id`
          ) //.select(`${query.model.table}.id`)
        })
      }
    })
  }

  if (searchParam.filters) mapParams(query, searchParam)
  if (searchParam.orders) mapOrders(query, searchParam)
}

function mapOrders(query, params) {
  params.orders.map((f) => {
    if (isSubfield(f.field)) {
      let relation = formatSubfield(f.field, query.model)
      query.orderBy(`${relation.tableName}.${relation.field}`, f.order)
    } else {
      query.orderBy(f.field, f.order)
    }
  })
}

function formatSubfield(fieldName, queryModel) {
  let data = fieldName.split('.')
  const relation = data[0]
  data.shift()
  const field = data.join('.')

  // const obj = queryModel.prototype[relation]()
  // const tableName = obj.$foreignTable
  const tableName = queryModel.prototype.constructor.table
  return {
    relation,
    field,
    tableName,
  }
}

function isSubfield(fieldName) {
  return fieldName.split('.').length - 1 > 0
}

function adjustValue(value) {
  if (value == null) return value

  if (typeof value === 'string' || value instanceof String)
    switch (value.toUpperCase()) {
      case '@FALSE':
        return false

      case '@TRUE':
        return true

      case '@NULL':
        return null

      default:
        return value
    }

  return value
}

function mapParams(query, params) {
  function toArray(value) {
    return Array.isArray(value) ? value : value.split(',')
  }

  // const model = query.model;
  params.filters.map((f) => {
    f.value = adjustValue(f.value)
    switch (f.operator.toUpperCase()) {
      case 'DOESNT_HAVE':
        if (isSubfield(f.field)) {
          let relation = formatSubfield(f.field, query.model)
          query.whereDoesntHave(relation.relation, (builder) => {
            if (f.value === null) builder.whereNull(`${relation.tableName}.${relation.field}`)
            else builder.where(`${relation.tableName}.${relation.field}`, '=', f.value)
          })
        } else {
          query.doesntHave((f.field = f.field == 'id' ? `${query.model.table}.id` : f.field))
        }
        break

      case 'HAS_MORE':
        query.has(f.field, '>', f.value)
        break

      case 'HAS_MORE_EQUAL':
        query.has(f.field, '>=', f.value)
        break

      case 'HAS':
        if (isSubfield(f.field)) {
          let relation = formatSubfield(f.field, query.model)
          query.whereHas(relation.relation, (builder) => {
            builder.has(relation.field)
          })
        } else {
          query.has((f.field = f.field == 'id' ? `${query.model.table}.id` : f.field))
        }
        break

      case 'IN':
        if (isSubfield(f.field)) {
          let relation = formatSubfield(f.field, query.model)
          query.whereHas(relation.relation, (builder) => {
            builder.whereIn(`${relation.tableName}.${relation.field}`, toArray(f.value))
          })
        } else {
          query.whereIn(
            (f.field = f.field == 'id' ? `${query.model.table}.id` : f.field),
            toArray(f.value)
          )
        }
        break

      case 'IS_NULL':
        if (isSubfield(f.field)) {
          let relation = formatSubfield(f.field, query.model)
          query.whereHas(relation.relation, (builder) => {
            builder.whereNull(`${relation.tableName}.${relation.field}`)
          })
        } else query.whereNull((f.field = f.field == 'id' ? `${query.model.table}.id` : f.field))
        break

      case 'IS_NOT_NULL':
        if (isSubfield(f.field)) {
          let relation = formatSubfield(f.field, query.model)
          query.whereHas(relation.relation, (builder) => {
            builder.whereNotNull(`${relation.tableName}.${relation.field}`)
          })
        } else {
          query.whereNotNull((f.field = f.field == 'id' ? `${query.model.table}.id` : f.field))
        }
        break

      case 'NOT_IN':
        if (isSubfield(f.field)) {
          let relation = formatSubfield(f.field, query.model)
          query.whereHas(relation.relation, (builder) => {
            builder.whereNotIn(`${relation.tableName}.${relation.field}`, toArray(f.value))
          })
        } else query.whereNotIn(f.field, toArray(f.value))
        break

      default:
        if (isSubfield(f.field)) {
          let relation = formatSubfield(f.field, query.model)
          query.whereHas(relation.relation, (builder) => {
            builder.where(`${relation.relation}.${relation.field}`, f.operator, f.value)
          })
        } else {
          if ((typeof f.value === 'string' || f.value instanceof String) && f.value[0] == '@')
            query.where(Database.raw(`${f.field} ${f.operator} ${f.value.substring(1)}`))
          else query.where(f.field, f.operator, f.value)
        }
    }
  })
}

export = {
  mapParams,
  prepareQuery,
  upsertWithTransaction,
}
