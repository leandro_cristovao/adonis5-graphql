// import Auth from '@ioc:Adonis/Addons/Auth'
import { TransactionClientContract } from '@ioc:Adonis/Lucid/Database'
import { BaseModel } from '@ioc:Adonis/Lucid/Orm'
const { prepareQuery } = require('../util')
export default class BaseRepository {
  model: typeof BaseModel

  constructor(model: typeof BaseModel) {
    this.model = model
  }

  async paginate(searchParam, _auth: any) {
    const query = this.model.query()
    prepareQuery(query, searchParam)
    const data = await query.paginate(searchParam.pagination.page, searchParam.pagination.perPage)
    return {
      pages: {
        total: data.total,
        perPage: data.perPage,
        page: data.currentPage,
        lastPage: data.lastPage,
      },

      rows: data.rows,
    }
  }

  async beforeUpsert(ctx) {
    let { input, relations } = ctx
    Object.getOwnPropertyNames(input).map((field) => {
      const typeName = typeof input[field]
      if (typeName.toString() == 'object') {
        relations.push({
          key: field,
          values: input[field],
        })

        delete input[field]
      }
    })
  }

  async afterUpsert(ctx) {
    let { relations, entity } = ctx
    for (const relation of relations) {
      let toInsert = relation.values.filter((x) => x.deleted == false).map((x) => x.id)
      let toDelete = relation.values.filter((x) => x.deleted == true).map((x) => x.id)

      if (toDelete.length) await entity.related(relation.key).detach([toDelete])
      if (toInsert.length) await entity.related(relation.key).sync(toInsert, false)
    }
  }

  async create(input, auth: any, trx: TransactionClientContract) {
    let relations = []
    await this.beforeUpsert({ input, auth, relations })
    const entity = await this.model.create(input)
    entity.useTransaction(trx)
    await this.afterUpsert({ input, entity, auth, relations })
    return entity
  }

  async merge(input, auth: any, trx: TransactionClientContract) {
    let relations = []
    await this.beforeUpsert({
      input,
      auth,
      relations,
    })
    const entity = await this.model.findOrFail(input.id, { client: trx })
    await entity.merge(input).save()
    await this.afterUpsert({ input, entity, auth, relations })

    return entity
  }
}
