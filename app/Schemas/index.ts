import requireDir = require('require-dir')

const schemas = requireDir('../Models', {
  recurse: false,
  extensions: ['.ts'],
})

let graphqlSchemas = ''
for (const objName in schemas) {
  graphqlSchemas += schemas[objName]?.graphQLSchema || ''
}

export const typeDef = `
# Indica a ordem do campo. ASC e DESC
enum EOrder {
  "Ascendente"
  ASC
  "Descendente"
  DESC
}

# Parâmetros sobre a paginação
input Pagination {
  # Página que deve ser retornada
  page: Int!

  # Total de registros por página
  perPage: Int!
}

# Filtros aplicados a busca
input Filter {
  # Nome interno do campo no banco de dados.
  field: String!

  # Operador da busca. Ex =, >=, <=, IN, NOT_IN
  operator: String!

  # Valor a ser pesquisado. No caso dos operadores IN/NOT_IN, valores devem ser passados separados por vírgula
  value: String!
}

# Parâmetros para ordenação do retorno dos dados
input Order {
  # Nome interno do campo no banco de dados.
  field: String!

  # Ordenador, ASC ou DESC
  order: EOrder!
}

# Parâmetros para busca de dados
input SearchParam {
  # Campos usados para filtro
  filters: [Filter]

  # Campos para ordenação do resultado
  orders: [Order]

  # Dados sobre o comportamento da paginação
  pagination: Pagination
}

# Informações sobre a paginação dos dados
type PaginationData {
  # Total de registros
  total: Int

  # Total de registros por página
  perPage: Int

  # Página atual
  page: Int

  # Última página
  lastPage: Int
}

${graphqlSchemas}
`
