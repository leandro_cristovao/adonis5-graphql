export default class AuthController {
  public async login({ auth, request, response }) {
    const email = request.input('email')
    const password = request.input('password')

    // response.type('json')
    try {
      const token = await auth.use('api').attempt(email, password)
      return token
    } catch {
      //https://github.com/adonisjs/http-server/blob/ea55c2a65fd388373d0b4e35ae45bee9cb096d02/src/Response/index.ts#L937-L1145
      return response.unauthorized({ error: 'Invalid login credentials' })
    }
  }

  public async logout({ auth, response }) {
    response.type('json')
    await auth.use('api').revoke()
    return {
      revoked: true,
    }
  }
}
