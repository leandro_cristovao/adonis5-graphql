import mutations from './Mutation'
import queries from './Query'
import resolvers from './Resolver'

module.exports = {
  Query: queries,
  Mutation: mutations,
  ...resolvers,
}
