import CourseUnit from 'App/Models/CourseUnit'
import BaseRepository from 'App/Repositories/BaseRepository'

module.exports = {
  CourseUnit: async (_, { searchParam }, { ctx }) => {
    const auth = ctx.auth

    const repository = new BaseRepository(CourseUnit)
    return await repository.paginate(searchParam, auth)
  },
}
