import Topic from 'App/Models/Topic'
import BaseRepository from 'App/Repositories/BaseRepository'

module.exports = {
  Topic: async (_, { searchParam }, { ctx }) => {
    const auth = ctx.auth

    const repository = new BaseRepository(Topic)
    return await repository.paginate(searchParam, auth)
  },
}
