import Question from 'App/Models/Question'
import BaseRepository from 'App/Repositories/BaseRepository'

module.exports = {
  Question: async (_, { searchParam }, { ctx }) => {
    const auth = ctx.auth

    const repository = new BaseRepository(Question)
    return await repository.paginate(searchParam, auth)
  },
}
