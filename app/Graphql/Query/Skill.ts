import Skill from 'App/Models/Skill'
import BaseRepository from 'App/Repositories/BaseRepository'

module.exports = {
  Skill: async (_, { searchParam }, { ctx }) => {
    const auth = ctx.auth

    const repository = new BaseRepository(Skill)
    return await repository.paginate(searchParam, auth)
  },
}
