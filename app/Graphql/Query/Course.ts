import Course from 'App/Models/Course'
import BaseRepository from 'App/Repositories/BaseRepository'

module.exports = {
  Course: async (_, { searchParam }, { ctx }) => {
    const auth = ctx.auth

    const repository = new BaseRepository(Course)
    return await repository.paginate(searchParam, auth)
  },
}
