import Role from 'App/Models/Role'
import BaseRepository from 'App/Repositories/BaseRepository'

module.exports = {
  Role: async (_, { searchParam }, { ctx }) => {
    const auth = ctx.auth

    const repository = new BaseRepository(Role)
    return await repository.paginate(searchParam, auth)
  },
}
