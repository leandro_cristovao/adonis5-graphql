import User from 'App/Models/User'
import BaseRepository from 'App/Repositories/BaseRepository'

module.exports = {
  User: async (_, { searchParam }, { ctx }) => {
    const auth = ctx.auth

    const repository = new BaseRepository(User)
    return await repository.paginate(searchParam, auth)
  },
}
