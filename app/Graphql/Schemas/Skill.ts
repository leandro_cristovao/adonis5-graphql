const { FieldId, FieldsBase, FieldName } = require('../utils')

export = `
type Skill {
  ${FieldsBase}
}

input SkillInput {
  ${FieldId}
  ${FieldName}
}

type SkillData {
  # Informações sobre a paginação dos dados
  pages: PaginationData!
  rows: [Skill]
}

type Query {
  Skill(searchParam: SearchParam): SkillData
}

type Mutation {
  upsertSkill(input: SkillInput): Skill
}
`
