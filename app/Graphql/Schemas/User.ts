const {
  FieldRole,
  FieldId,
  FieldEmail,
  FieldPassword,
  FieldSlug,
  FieldsBase,
  FieldName,
  InputRole,
} = require('../utils')

export = `
type User {
  ${FieldsBase}
  ${FieldEmail}
  ${FieldRole}
}

input UserInput {
  ${FieldId}
  ${FieldName}!
  ${FieldSlug}
  ${FieldEmail}!
  ${FieldPassword}
  ${InputRole}
}

type UserData {
  # Informações sobre a paginação dos dados
  pages: PaginationData!
  rows: [User]
}

type Query {
  User(searchParam: SearchParam): UserData
}

type Mutation {
  upsertUser(input: UserInput): User
}
`
