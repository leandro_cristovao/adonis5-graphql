const { FieldId, FieldSlug, FieldsBase, FieldName } = require('../utils')

export = `
type Role {
  ${FieldsBase}
  ${FieldSlug}
}

input RoleInput {
  ${FieldId}
  ${FieldName}!
  ${FieldSlug}
}

type RoleData {
  # Informações sobre a paginação dos dados
  pages: PaginationData!
  rows: [Role]
}

type Query {
  Role(searchParam: SearchParam): RoleData
}

type Mutation {
  upsertRole(input: RoleInput): Role
}
`
