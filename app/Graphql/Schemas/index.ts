import requireDir = require('require-dir')

const schemas = requireDir('../../Models', {
  recurse: false,
  extensions: ['.ts'],
})

let graphqlSchemas = ''
for (const objName in schemas) {
  graphqlSchemas += schemas[objName]?.graphQLSchema || ''
}

export const typeDef = `
# Tipo de conteúdo da questão. Hoje só temos a opção TEXTO
enum EQuestionContentType {
  "Texto"
  TEXT
}

# Tipo de questão
enum EQuestionType {
  "Diagnótico"
  DIAGNOSIS
  "Discursiva"
  DISCURSIVE
  "Objetiva"
  OBJECTIVE
  "Prática"
  PRACTICE
}

# Tipo de dificuldade da questão
enum EQuestionLevel {
  "Básico"
  BASIC
  "Intermediário"
  INTERMEDIATE
  "Avançado"
  ADVANCED
}

# Status da questão
enum EQuestionStatus {
  "Novo"
  NEW
  "Análise Educacional"
  EDUCATIONAL_ANALYSIS
  "Ajustes"
  ADJUSTMENT
  "Esperando Análise Educacional"
  WAITING_AE
  "Enviado para Revisão Técnica"
  SEND_TO_RT
  "Revisão Técnica"
  RT
  "Validação da Revisão Técnica"
  RT_VALIDATION
  "Finalizada sem uso"
  FINISHED_WITHOUT_USE
  "Finalizada"
  FINISHED
}

# Indica a ordem do campo. ASC e DESC
enum EOrder {
  "Ascendente"
  ASC
  "Descendente"
  DESC
}

# Parâmetros sobre a paginação
input Pagination {
  # Página que deve ser retornada
  page: Int!

  # Total de registros por página
  perPage: Int!
}

# Filtros aplicados a busca
input Filter {
  # Nome interno do campo no banco de dados.
  field: String!

  # Operador da busca. Ex =, >=, <=, IN, NOT_IN
  operator: String!

  # Valor a ser pesquisado. No caso dos operadores IN/NOT_IN, valores devem ser passados separados por vírgula
  value: String!
}

# Parâmetros para ordenação do retorno dos dados
input Order {
  # Nome interno do campo no banco de dados.
  field: String!

  # Ordenador, ASC ou DESC
  order: EOrder!
}

# Parâmetros para busca de dados
input SearchParam {
  # Campos usados para filtro
  filters: [Filter]

  # Campos para ordenação do resultado
  orders: [Order]

  # Dados sobre o comportamento da paginação
  pagination: Pagination
}

# Informações sobre a paginação dos dados
type PaginationData {
  # Total de registros
  total: Int

  # Total de registros por página
  perPage: Int

  # Página atual
  page: Int

  # Última página
  lastPage: Int
}

# Representa um registro que deve ser relacionado ao registro atual
input RelationDataInput {
  # Id interno do model a ser referenciado
  id: Int!

  # Śe TRUE, indica que relação deve ser removida
  deleted: Boolean!
}

`
