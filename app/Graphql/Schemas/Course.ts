const { FieldId, FieldsBase, FieldName } = require('../utils')

export = `
type Course {
  ${FieldsBase}
}

input CourseInput {
  ${FieldId}
  ${FieldName}
}

type CourseData {
  # Informações sobre a paginação dos dados
  pages: PaginationData!
  rows: [Course]
}

type Query {
  Course(searchParam: SearchParam): CourseData
}

type Mutation {
  upsertCourse(input: CourseInput): Course
}
`
