const {
  FieldDeleted,
  FieldsBase,
  FieldId,
  FieldName,
  FieldSlug,
  FieldTopics,
  InputTopics,
} = require('../utils')

export = `
type CourseUnit { 
  ${FieldsBase}
  ${FieldSlug}
  ${FieldTopics}
}

input CourseUnitInput {
  ${FieldId}
  ${FieldName}
  ${FieldSlug}
  ${InputTopics}
  ${FieldDeleted}
}

type CourseUnitData {
  # Informações sobre a paginação dos dados
  pages: PaginationData!
  rows: [CourseUnit]
}

type Query {
  CourseUnit(searchParam: SearchParam): CourseUnitData
}

type Mutation {
  upsertCourseUnit(input: CourseUnitInput): CourseUnit
}
`
