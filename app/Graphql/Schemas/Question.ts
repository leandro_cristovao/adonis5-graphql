const {
  FieldLastSemester,
  FieldIsNew,
  FieldLevel,
  FieldQuestionStatus,
  FieldReference,
  FieldQuestionType,
  FieldContentType,
  FieldContent,
  FieldId,
  FieldsBase,
  FieldName,
  InputSkills,
  InputCourse,
  FieldSkills,
  FieldCourse,
} = require('../utils')

module.exports = `
type Question {
  ${FieldCourse}
  ${FieldContent}
  ${FieldContentType}
  ${FieldIsNew}
  ${FieldLastSemester}
  ${FieldLevel}
  ${FieldQuestionStatus}
  ${FieldQuestionType}
  ${FieldReference}
  ${FieldsBase}
  ${FieldSkills} 
}

input QuestionInput {
  ${FieldId}
  ${FieldName}
  ${FieldContent}
  ${FieldLevel}
  ${FieldQuestionStatus}
  ${FieldQuestionType}
  ${FieldReference}
  ${InputSkills}
  ${InputCourse}
}

type QuestionData {
  # Informações sobre a paginação dos dados
  pages: PaginationData!
  rows: [Question]
}

type Query {
  Question(searchParam: SearchParam): QuestionData
}

type Mutation {
  upsertQuestion(input: QuestionInput): Question
}`
