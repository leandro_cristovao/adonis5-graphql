const { FieldId, FieldsBase, FieldName, FieldSkills, InputSkills } = require('../utils')

export = `
type Topic {
  ${FieldsBase}
  ${FieldSkills}
}

input TopicInput {
  ${FieldId}
  ${FieldName}
  ${InputSkills}
}

type TopicData {
  # Informações sobre a paginação dos dados
  pages: PaginationData!
  rows: [Topic]
}

type Query {
  Topic(searchParam: SearchParam): TopicData
}

type Mutation {
  upsertTopic(input: TopicInput): Topic
}
`
