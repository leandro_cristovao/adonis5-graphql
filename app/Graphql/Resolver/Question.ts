const { course, createdUser, skills } = require('./utils')
export = {
  course,
  createdUser,
  skills,
}
