import Course from 'App/Models/Course'
import Role from 'App/Models/Role'
import User from 'App/Models/User'
const { prepareQuery } = require('../../../util')

export = {
  createdUser: async (entity) => {
    if (!entity.created_user_id) return null
    return await User.find(entity.created_user_id)
  },
  updatedUser: async (entity) => {
    if (!entity.updated_user_id) return null
    return await User.find(entity.updated_user_id)
  },
  deletedUser: async (entity) => {
    if (!entity.deleted_user_id) return null
    return await User.find(entity.deleted_user_id)
  },
  role: async (entity) => {
    if (!entity.role_id) return null
    return await Role.find(entity.role_id)
  },
  course: async (entity) => {
    if (!entity.course_id) return null
    return await Course.find(entity.course_id)
  },
  skills: async (entity, { searchParam }) => {
    const query = entity.related('skills').query()
    prepareQuery(query, searchParam)
    let data = await query.paginate(searchParam.pagination.page, searchParam.pagination.perPage)
    return data
  },
  topics: async (entity, { searchParam }) => {
    const query = entity.related('topics').query()
    prepareQuery(query, searchParam)
    let data = await query.paginate(searchParam.pagination.page, searchParam.pagination.perPage)
    return data
  },
}
