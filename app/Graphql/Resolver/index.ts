import requireDir = require('require-dir')

const methods = requireDir('.', {
  recurse: false,
  extensions: ['.ts'],
})

export default methods
