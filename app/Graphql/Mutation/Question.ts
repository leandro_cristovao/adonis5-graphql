import Question from 'App/Models/Question'
import BaseRepository from 'App/Repositories/BaseRepository'
const { upsertWithTransaction } = require('../../util.ts')

module.exports = {
  async upsertQuestion(_, { input }, { ctx }) {
    return await upsertWithTransaction(new BaseRepository(Question), input, ctx.auth)
  },
}
