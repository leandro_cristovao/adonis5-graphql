import Topic from 'App/Models/Topic'
import BaseRepository from 'App/Repositories/BaseRepository'
const { upsertWithTransaction } = require('../../util.ts')

module.exports = {
  async upsertTopic(_, { input }, { ctx }) {
    return await upsertWithTransaction(new BaseRepository(Topic), input, ctx.auth)
  },
}
