import User from 'App/Models/User'
import BaseRepository from 'App/Repositories/BaseRepository'
const { upsertWithTransaction } = require('../../util.ts')

module.exports = {
  async upsertUser(_, { input }, { ctx }) {
    return await upsertWithTransaction(new BaseRepository(User), input, ctx.auth)
  },
}
