import requireDir = require('require-dir')

const entities = requireDir('.', {
  recurse: false,
  extensions: ['.ts'],
})

const methods = {}
for (let entityName in entities) {
  const obj = entities[entityName]

  let functionNames = Object.getOwnPropertyNames(obj)
  functionNames.map((f) => {
    methods[f] = obj[f]
  })
}

export default methods
