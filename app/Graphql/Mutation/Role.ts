import Role from 'App/Models/Role'
import BaseRepository from 'App/Repositories/BaseRepository'
const { upsertWithTransaction } = require('../../util.ts')

module.exports = {
  async upsertRole(_, { input }, { ctx }) {
    return await upsertWithTransaction(new BaseRepository(Role), input, ctx.auth)
  },
}
