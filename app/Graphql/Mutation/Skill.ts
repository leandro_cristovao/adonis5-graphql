import Skill from 'App/Models/Skill'
import BaseRepository from 'App/Repositories/BaseRepository'
const { upsertWithTransaction } = require('../../util.ts')

module.exports = {
  async upsertSkill(_, { input }, { ctx }) {
    return await upsertWithTransaction(new BaseRepository(Skill), input, ctx.auth)
  },
}
