import Course from 'App/Models/Course'
import BaseRepository from 'App/Repositories/BaseRepository'
const { upsertWithTransaction } = require('../../util.ts')

module.exports = {
  async upsertCourse(_, { input }, { ctx }) {
    return await upsertWithTransaction(new BaseRepository(Course), input, ctx.auth)
  },
}
