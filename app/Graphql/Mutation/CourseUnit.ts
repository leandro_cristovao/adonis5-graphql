import CourseUnit from 'App/Models/CourseUnit'
import BaseRepository from 'App/Repositories/BaseRepository'
const { upsertWithTransaction } = require('../../util.ts')

module.exports = {
  async upsertCourseUnit(_, { input }, { ctx }) {
    return await upsertWithTransaction(new BaseRepository(CourseUnit), input, ctx.auth)
  },
}
