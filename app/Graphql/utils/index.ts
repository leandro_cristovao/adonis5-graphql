module.exports = {
  FieldContent: `
   # Conteúdo
   content: String`,
  FieldContentType: `
  # Tipo de conteúdo
  contentType: EQuestionContentType`,
  FieldCourse: `
  # Curso
  course: Course`,
  FieldDeleted: `
  # Indica se registro está desativado
  deleted: Boolean
  `,
  FieldEmail: `
  # Email
  email: String`,
  FieldId: `
  # ID da entidade
  id: Int`,
  FieldIsNew: `
  # Indica se a questão já foi usada alguma vez
  isNew: Boolean`,
  FieldLastSemester: `
  # Indica o último semestre que a questão foi usada 
  lastSemester: Int`,
  FieldLevel: `
  # Nível da questão
  level: EQuestionLevel`,
  FieldName: `
  # Nome da entidade
  name: String`,
  FieldPassword: `
  # Senha
  password: String`,
  FieldQuestionStatus: `
  # Status da questão
  status: EQuestionStatus`,
  FieldQuestionType: `
  # Tipo de questão
  type: EQuestionType`,
  FieldReference: `
  # Referência com Excel
  reference: String`,
  FieldRole: `
  # Role do usuário
  role: Role
  `,
  FieldsBase: `
  # Chave primária
  id: Int

  # Nome da entidade
  name: String

  # Se true, registro removido
  deleted: Boolean

  # Usuário que criou o registro
  createdUser: User
  `,

  /**
   * Fields
   */
  FieldSkills: `
  # Habilidades relacionadas
  skills(searchParam: SearchParam!): SkillData`,
  FieldSlug: `
  # Nome interno
  slug: String
  `,
  FieldTopics: `
  # Tópicos relacionadas
  topics(searchParam: SearchParam!): TopicData`,
  InputCourse: `
  # Curso
  course_id: Int`,
  InputRole: `
  # Role
  role_id: Int
  `,
  InputSkills: `
  # Listagem de Skills para inserção/remoção
  skills: [RelationDataInput]
  `,
  InputTopics: `
  # Listagem de tópicos para inserção/remoção
  topics: [RelationDataInput]
  `,
}
