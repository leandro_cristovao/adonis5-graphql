import { column, manyToMany, ManyToMany } from '@ioc:Adonis/Lucid/Orm'
import Topic from './Topic'
import BaseTenantModel from './BaseTenantModel'

export default class CourseUnit extends BaseTenantModel {
  @column()
  public slug: string

  @manyToMany(() => Topic, {
    pivotTable: 'course_unit_topics',
  })
  public topics: ManyToMany<typeof Topic>
}
