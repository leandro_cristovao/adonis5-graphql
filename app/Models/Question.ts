import {
  column,
  HasMany,
  hasMany,
  HasOne,
  hasOne,
  manyToMany,
  ManyToMany,
} from '@ioc:Adonis/Lucid/Orm'
import Answer from './Answer'
import BaseTenantModel from './BaseTenantModel'
import Course from './Course'
import EvaluationType from './EvaluationType'
import CourseUnit from './CourseUnit'
import Skill from './Skill'
export default class Question extends BaseTenantModel {
  @column()
  public content: string
  @column()
  public contentType: string
  @column()
  public type: string
  @column()
  public reference: string
  @column()
  public level: string
  @column()
  public status: string
  @column()
  public isNew: boolean
  @column()
  public lastSemester: boolean

  @hasMany(() => Answer)
  public answers: HasMany<typeof Answer>

  @manyToMany(() => Skill, {
    //todo checar se tem como pegar a tenant
    // onQuery(query) {
    //   query.where('tenant_id', 123)
    // },
    pivotTable: 'question_skills',
  })
  public skills: ManyToMany<typeof Skill>

  @hasOne(() => Course)
  public course: HasOne<typeof Course>

  @hasOne(() => CourseUnit)
  public CourseUnit: HasOne<typeof CourseUnit>

  @hasOne(() => EvaluationType)
  public evaluationType: HasOne<typeof EvaluationType>
  @column()
  public course_id: number
}
