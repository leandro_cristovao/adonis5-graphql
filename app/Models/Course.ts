import { column } from '@ioc:Adonis/Lucid/Orm'
import BaseTenantModel from './BaseTenantModel'

export default class Course extends BaseTenantModel {
  //precisa ter ao menos uma column, senao da erro...
  @column()
  public content: string
}
