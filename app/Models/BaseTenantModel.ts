import { DateTime } from 'luxon'
import { SnakeCaseNamingStrategy, BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

/**
 * Por padrao quero que os campos tenham os nomes extamente como definidos
 */
class CustomCaseNamingStrategy extends SnakeCaseNamingStrategy {
  public columnName(_model: typeof BaseModel, propertyName: string) {
    return propertyName
  }
}
BaseModel.namingStrategy = new CustomCaseNamingStrategy()

export default class BaseTenantModel extends BaseModel {
  // public serializeExtras = true

  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @column()
  public tenant_id: number

  @column()
  public created_user_id: number

  @column()
  public deleted: boolean
}
