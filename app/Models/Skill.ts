import { column } from '@ioc:Adonis/Lucid/Orm'
import BaseTenantModel from './BaseTenantModel'

export default class Skill extends BaseTenantModel {
  @column()
  public content: string
}
