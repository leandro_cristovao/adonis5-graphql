import Hash from '@ioc:Adonis/Core/Hash'
import { column, beforeSave, belongsTo, BelongsTo } from '@ioc:Adonis/Lucid/Orm'
import Role from './Role'
import BaseTenantModel from './BaseTenantModel'
export default class User extends BaseTenantModel {
  @column()
  public email: string

  @column({ serializeAs: null })
  public password: string

  @column()
  public rememberMeToken?: string

  @belongsTo(() => Role)
  public role: BelongsTo<typeof Role>
  @column()
  public role_id: number

  @beforeSave()
  public static async hashPassword(user: User) {
    if (user.$dirty.password) {
      user.password = await Hash.make(user.password)
    }
  }
}
