import { hasOne, HasOne, manyToMany, ManyToMany } from '@ioc:Adonis/Lucid/Orm'
import Skill from './Skill'
import BaseTenantModel from './BaseTenantModel'

export default class Topic extends BaseTenantModel {
  @hasOne(() => Topic)
  public course: HasOne<typeof Topic>

  @manyToMany(() => Skill, {
    pivotTable: 'topic_skills',
  })
  public skills: ManyToMany<typeof Skill>
}
