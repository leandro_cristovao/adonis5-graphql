import { hasOne, HasOne } from '@ioc:Adonis/Lucid/Orm'
import BaseTenantModel from './BaseTenantModel'
import Question from './Question'

export default class Answer extends BaseTenantModel {
  @hasOne(() => Question)
  public question: HasOne<typeof Question>
}
