import BaseTenantModel from './BaseTenantModel'
import { beforeSave, column } from '@ioc:Adonis/Lucid/Orm'
import { string } from '@ioc:Adonis/Core/Helpers'
export default class Role extends BaseTenantModel {
  public static table = 'roles'

  @column()
  public slug: string

  @beforeSave()
  public static async hashPassword(entity: Role) {
    if (!entity.slug) entity.slug = string.snakeCase(entity.name)
    entity.slug = entity.slug.toUpperCase()
  }
}
