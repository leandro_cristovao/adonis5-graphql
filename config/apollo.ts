import { ApolloConfig, ApolloBaseContext } from '@ioc:Apollo/Config'

interface ApolloContext extends ApolloBaseContext {
  // Define here what will be available in the GraphQL context
}

const config: ApolloConfig = {
  schemas: 'app/Graphql/Schemas',
  resolvers: 'app/Graphql',
  path: '/graphql',
  apolloServer: {
    context: ({ ctx }): ApolloContext => {
      return { ctx }
    },
  },
  executableSchema: {
    inheritResolversFromInterfaces: true,
  },
}

export default config
